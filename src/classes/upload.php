<?php
namespace FAE\uploader;

use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route;

use FAE\rest\rest;

class upload {
  
  static $defaults = [
    'randomise' => 64,
    'paramName' => 'files',
    'mkdirMode' => 0755,
    'uploadDir' => __DIR__.'/../../../../upload/',
    'filePath' => 'upload/',
    'discardAbortedUploads' => true,
    'inputStream' => 'php://input',
    'maxFileSize' => null,
    'minFileSize' => 1,
    'acceptFileTypes' => '/.+$/i',
    'acceptFileName' => '0-9A-Za-z\$\-\_\.\+\!\*\'\(\)\,',
    'downloadViaPhp' => 1,
    'inlineFileTypes' => '/\.(gif|jpe?g|png)$/i',
    'deleteType' => 'DELETE',
  ];
  
  // PHP File Upload error message codes:
  // http://php.net/manual/en/features.file-upload.errors.php
  static $errors = [
      1                     => 'Your file is too large to be uploaded.',
      2                     => 'Your file is too large to be uploaded.',
      3                     => 'The uploaded file was only partially uploaded',
      4                     => 'No file was uploaded',
      6                     => 'Missing a temporary folder',
      7                     => 'Failed to write file to disk',
      8                     => 'A PHP extension stopped the file upload',
      'post_max_size'       => 'Your file is too large to be uploaded.',
      'max_file_size'       => 'Your file is too large to be uploaded.',
      'min_file_size'       => 'File is too small',
      'accept_file_types'   => 'Filetype not allowed',
      'max_number_of_files' => 'Maximum number of files exceeded',
      'max_width'           => 'Image exceeds maximum width',
      'min_width'           => 'Image requires a minimum width',
      'max_height'          => 'Image exceeds maximum height',
      'min_height'          => 'Image requires a minimum height',
      'abort'               => 'File upload aborted',
      'image_resize'        => 'Failed to resize image',
      'permission'          => 'You do not have permission to upload to the uploads folder',
  ];
  
  var $options;
  
  function __construct( array $options )
  {
    $this->options = ($options + self::$defaults);
  }
  
  static function action( array $variables )
  {
    $instance = new self( $variables );
    switch($instance->options['_action']){
      case 'read':
        $instance->load( $instance->options['file'] );
        break;
      case 'create':
        $instance->upload( $_FILES[$instance->options['paramName']] );
        break;
      case 'delete':
        $instance->delete( $_FILES[$instance->options['paramName']] );
        break;
    }
  }
  
  static function routes( string $routePath = '/api', array $variables = [], bool $readRoute = true )
  {
    $rc = new RouteCollection();
    $variables = $variables + self::$defaults;
    $rc->add( 'uploader'.str_replace('/', '-', $routePath).'-create', new Route(
        $routePath.'/upload',
        [
          '_action'     => 'create',
          '_controller' => '\\FAE\\uploader\\upload::action',
        ] + $variables,
        [],
        [],
        '',
        [],
        ['POST','PATCH','PUT']
      )
    );
    if($readRoute){
      $rc->add( 'uploader'.str_replace('/', '-', $routePath).'-read', new Route(
          '/'.$variables['filePath'].'{file}',
          [
            '_action'     => 'read',
            '_controller' => '\\FAE\\uploader\\upload::action',
          ] + $variables,
          ['file' => '['.$variables['acceptFileName'].']+\.[A-Za-z]{2,20}'],
          [],
          '',
          [],
          ['GET']
        )
      );
    }
    /*$rc->add( 'uploader'.str_replace('/', '-', $routePath).'-delete', new Route(
        '/upload',
        [
          '_action'     => 'delete',
          '_controller' => '\\FAE\\uploader\\upload::action',
        ] + $variables,
        [],
        [],
        '',
        [],
        ['DELETE']
      )
    );*/
    $rc->setSchemes(['https','http']);
    return $rc;
  }
  
  function load( string $file )
  {
     switch ($this->options['downloadViaPhp']) {
      case 1:
        $redirect_header = null;
        break;
      case 2:
        $redirect_header = 'X-Sendfile';
        break;
      case 3:
        $redirect_header = 'X-Accel-Redirect';
        break;
      default:
        return header('HTTP/1.1 403 Forbidden');
    }
    if( !file_exists($this->options['uploadDir'].$file) ){
      return header('HTTP/1.1 404 Not Found');
    }
    if($redirect_header){
      return header($redirect_header.': '.$this->getDownloadUrl($file));
    }
    $file_path = $this->options['uploadDir'].$file;
    // Prevent browsers from MIME-sniffing the content-type:
    header('X-Content-Type-Options: nosniff');
    if( !preg_match( $this->options['inlineFileTypes'], $file ) ){
      header('Content-Type: application/octet-stream');
      header('Content-Disposition: attachment; filename="'.$file.'"');
    } else {
      header('Content-Type: '.mime_content_type($file_path));
      header('Content-Disposition: inline; filename="'.$file.'"');
    }
    header('Content-Length: '.$this->getFileSize($file_path));
    header('Last-Modified: '.gmdate('D, d M Y H:i:s T', filemtime($file_path)));
    readfile($file_path);
  }
  
  function upload( array $upload )
  {
    // Parse the Content-Range header, which has the following form:
    // Content-Range: bytes 0-524287/2000000
    $contentRangeHeader   = $_SERVER['HTTP_CONTENT_RANGE'];
    $contentRange         = $contentRangeHeader ? preg_split('/[^0-9]+/', $contentRangeHeader) : null;
    $size                 = $contentRange ? $contentRangeHeader[3] : null;
    $files                = [];
    
    if($upload){
      if(is_array($upload['tmp_name'])){
        // paramName is an array identifier like "files[]",
        // $upload is a multi-dimensional array:
        foreach($upload['tmp_name'] as $index => $value){
          $files[] = $this->handleFileUpload(
            $upload['tmp_name'][$index],
            $this->processFileName( $upload['name'][$index] ),
            $size ? $size : $upload['size'][$index],
            $upload['type'][$index],
            $upload['error'][$index],
            $index,
            $content_range
          );
        }
      } else {
        // paramName is a single object identifier like "file",
        // $upload is a one-dimensional array:
        $files[] = $this->handleFileUpload(
          isset($upload['tmp_name']) ? $upload['tmp_name'] : null,
          $this->processFileName( $upload['name'] ),
          $size ? $size : ( isset($upload['size']) ? $upload['size'] : $_SERVER['CONTENT_LENGTH'] ),
          isset($upload['type']) ? $upload['type'] : $_SERVER['CONTENT_TYPE'],
          isset($upload['error']) ? $upload['error'] : null,
          null,
          $content_range
        );
      }
    }
    
    $response = array($this->options['paramName'] => $files);
    return $this->output( rest::formatData( $files ) );
  }
  
  protected function randGen( int $length = 10, string $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ' )
  {
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
      $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
  }
  
  protected function handleFileUpload( string $uploaded_file, string $name, int $size, string $type, $error, $index = null, $content_range = null )
  {
    $file = new \stdClass();
    // Add original into the returned object
    $file->orig = $name;
    $file->name = $name;
    $file->size = $this->fixIntegerOverflow( (int) $size );
    $file->type = $type;
    
    if($this->options['randomise']){
      $file->name = $this->randGen($this->options['randomise']).'.'.end(explode('.', $file->orig));
    }
    
    if($this->validate($uploaded_file, $file, $error, $index)){
      
      $upload_dir = $this->options['uploadDir'];
      if(!is_dir($upload_dir)){
        mkdir($upload_dir, $this->options['mkdirMode'], true);
      }
      
      $file_path = $this->getUploadPath($file->name);
      $append_file = $content_range && is_file($file_path) && $file->size > $this->getFileSize($file_path);
      
      if($uploaded_file && is_uploaded_file($uploaded_file)){
        // multipart/formdata uploads (POST method uploads)
        if($append_file){
          file_put_contents(
            $file_path,
            fopen($uploaded_file, 'r'),
            FILE_APPEND
          );
        } else {
          if(!move_uploaded_file($uploaded_file, $file_path)){
            return $this->getError('permission');
          }
        }
      } else {
        // Non-multipart uploads (PUT method support)
        file_put_contents(
          $file_path,
          fopen($this->options['inputStream'], 'r'),
          $append_file ? FILE_APPEND : 0
        );
      }
      $file_size = $this->getFileSize($file_path, $append_file);
      if($file_size === $file->size){
        $file->url = $this->getDownloadUrl($file->name);
        /*if($this->is_valid_image_file($file_path)){
          $this->handle_image_file($file_path, $file);
        }*/
      } else {
        $file->size = $file_size;
        if(!$content_range && $this->options['discardAbortedUploads']){
          unlink($file_path);
          $file->error = $this->getError('abort');
        }
      }
      $this->extendFile($file);
      
    } else {
      if(isset($_SESSION['fileupload'][$file->orig])){
        unset($_SESSION['fileupload'][$file->orig]);
      }
    }
    return $file;
  }
  
  protected function validate($uploaded_file, $file, $error, $index)
  {
    if($error){
      $file->error = $this->getError($error);
      return false;
    }
    $content_length = $this->fixIntegerOverflow( (int) $_SERVER['CONTENT_LENGTH'] );
    $post_max_size = $this->standardSize(ini_get('post_max_size'));
    if($post_max_size && ($content_length > $post_max_size)){
        $file->error = $this->getError('post_max_size');
        return false;
    }
    if(!preg_match($this->options['acceptFileTypes'], $file->name)){
      $file->error = $this->getError('accept_file_types');
      return false;
    }
    if($uploaded_file && is_uploaded_file($uploaded_file)){
      $file_size = $this->getFileSize($uploaded_file);
    } else {
      $file_size = $content_length;
    }
    if($this->options['maxFileSize'] && (
            $file_size > $this->options['maxFileSize'] ||
            $file->size > $this->options['maxFileSize'])
        ){
        $file->error = $this->getError('maxFileSize');
        return false;
    }
    if($this->options['minFileSize'] &&
        $file_size < $this->options['minFileSize']){
        $file->error = $this->getError('minFileSize');
        return false;
    }
    if(is_int($this->options['max_number_of_files']) &&
      ($this->count_file_objects() >= $this->options['max_number_of_files']) &&
      // Ignore additional chunks of existing files:
      !is_file($this->options['uploadDir'].$file->name)
    ){
      $file->error = $this->getError('max_number_of_files');
      return false;
    }
    return true;
  }
  
  function delete( bool $print_response = true )
  {
    $file_names = $this->get_file_names_params();
    if(empty($file_names)){
      $file_names = array($this->get_file_name_param());
    }
    $response = array();
    foreach($file_names as $file_name){
      $file_path = $this->options['uploadDir'].$file_name;
      $success = is_file($file_path) && $file_name[0] !== '.' && unlink($file_path);
      if($success){
        foreach($this->options['image_versions'] as $version => $options){
          if(!empty($version)){
            $file = $this->options['uploadDir'].$file_name;
            if(is_file($file)){
              unlink($file);
            }
          }
        }
      }
      $response[$file_name] = $success;
    }
    return $this->generate_response($response, $print_response);
  }
  
  protected function output( $content )
  {
    $json = json_encode($content);
    $redirect = stripslashes($_POST['redirect']);
    if( $redirect && preg_match( $this->options['redirectAllowTarget'], $redirect ) ){
      header('Location: '.sprintf($redirect, rawurlencode($json)));
      return;
    }
    header('Pragma: no-cache');
    header('Cache-Control: no-store, no-cache, must-revalidate');
    header('Content-Disposition: inline; filename="files.json"');
    // Prevent Internet Explorer from MIME-sniffing the content-type:
    header('X-Content-Type-Options: nosniff');
    if ($this->options['accessControlAllowOrigin']) {
      $this->send_access_control_headers();
    }
    header('Vary: Accept');
    if (strpos($_SERVER['HTTP_ACCEPT'], 'application/json') !== false) {
      header('Content-type: application/json');
    } else {
      header('Content-type: text/plain');
    }
    if($_SERVER['HTTP_CONTENT_RANGE']) {
      $files = isset($content[$this->options['paramName']]) ? $content[$this->options['paramName']] : null;
      if( $files && is_array($files) && is_object($files[0]) && $files[0]->size ){
        header('Range: 0-'.($this->fixIntegerOverflow((int)$files[0]->size) - 1));
      }
    }
    echo $json;
  }
  
  function processFileName( string $fileName )
  {
    return preg_replace('/[^'.$this->options['acceptFileName'].']/', '', $fileName);
  }
  
  protected function extendFile($file) {
    global $config;
    /*$file->deleteUrl = $config->root.$config->path.'/'.;
    $file->deleteType = $this->options['deleteType'];
    if($file->deleteType !== 'DELETE') {
      $file->deleteUrl .= '&_method=DELETE';
    }
    if($this->options['accessControlAllowCredentials']) {
      $file->deleteWithCredentials = true;
    }*/
  }
  
  protected function getDownloadUrl( string $filename )
  {
    global $config;
    return $config->root.$config->path.'/'.$this->options['filePath'].urlencode($filename);
  }
  
  protected function getUploadPath( string $filename )
  {
    return $this->options['uploadDir'].urlencode($filename);
  }
  
  protected function getError( $error )
  {
    return self::$errors[$error];
  }

  // Fix for overflowing signed 32 bit integers,
  // works for sizes up to 2^32-1 bytes (4 GiB - 1):
  protected function fixIntegerOverflow($size)
  {
    if($size < 0){
      $size += 2.0 * (PHP_INT_MAX + 1);
    }
    return $size;
  }
  
  protected function getFileSize( string $path, bool $clear = false )
  {
    if($clear){
      clearstatcache(true, $path);
    }
    return $this->fixIntegerOverflow(filesize($path));
  }

  public function standardSize($val)
  {
    $val = trim($val);
    $last = strtolower($val[strlen($val)-1]);
    $val = (int)$val;
    switch($last){
      case 't':
        $val *= 1024;
      case 'g':
        $val *= 1024;
      case 'm':
        $val *= 1024;
      case 'k':
        $val *= 1024;
    }
    return $this->fixIntegerOverflow($val);
  }
}